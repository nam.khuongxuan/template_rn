import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import staticResources from './staticResources/reducer';

const rootReducer = combineReducers({
  staticResources: staticResources,
  form: formReducer,
});

export default rootReducer;
