import { useMemo, useCallback } from 'react';
import { useSelector } from 'react-redux';
import jwt from 'jwt-decode';
import isString from 'lodash/isString';
import isArray from 'lodash/isArray';
import intersection from 'lodash/intersection';

const usePermissions = (initialValue) => {
  const token = useSelector((state) => state?.auth?.orgSession?.access_token);

  const decoded = token ? jwt(token) : undefined;
  const userPermissions = decoded?.authorities;

  const checkPermissions = useCallback(
    (permissions) => {
      if (isString(permissions)) {
        return userPermissions.includes(permissions);
      } else if (isArray(permissions)) {
        return (
          intersection(userPermissions, permissions).length ===
          permissions.length
        );
      }
    },
    [userPermissions],
  );

  const isAllowed = useMemo(() => {
    if (initialValue?.orPermissions) {
      for (let pers of initialValue.orPermissions) {
        if (checkPermissions(pers)) {
          return true;
        }
      }
    }

    if (initialValue?.andPermissions) {
      return checkPermissions(initialValue.andPermissions);
    }

    return false;
  }, [checkPermissions, initialValue]);

  return isAllowed;
};

export default usePermissions;
