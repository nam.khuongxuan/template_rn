import React from 'react';
import { StyleSheet } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
// import Typography from '@src/components/Typography';
// import colors from '@src/utils/colors';
// import Box from '@src/components/Box';

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    // height: '100%',
  },
});

const Map = (props) => {
  const location = props?.currentLocation
    ? {
        longitude: Number(props.currentLocation.longitude),
        latitude: Number(props.currentLocation.latitude),
      }
    : null;
  // console.log(location);

  return (
    <MapView
      provider={PROVIDER_GOOGLE}
      style={styles.map}
      region={{
        latitude: location.latitude,
        longitude: location.longitude,
        latitudeDelta: 0.005,
        longitudeDelta: 0.005,
      }}>
      <Marker coordinate={location} />
    </MapView>
  );
};

export default Map;
