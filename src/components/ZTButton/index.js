import React from 'react';
import { StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import {
  SHADOW_HEIGHT_MAP,
  SHADOW_OPACITY_MAP,
  SHADOW_RADIUS_MAP,
} from './constants';
import { normalizeOptions } from '@src/utils/formatters';
import Box from '../Box';
import colors from '@src/utils/colors';
import Typography from '../Typography';

const ZTButton = ({
  style,
  children,
  margin,
  padding,
  prefix,
  suffix,
  title,
  type,
  middleBox,
  disabled,
  loading,
  ...rest
}) => {
  let buttonType = type || 'default';

  const combinedStyle = [
    'flexDirection',
    'justify',
    'align',
    'alignSelf',
    'flex',
    'background',
    'square',
    'circle',
    'shadowDepth',
  ]
    .map((e) => {
      if (!rest[e]) {
        return;
      }

      return styles[e](rest[e]);
    })
    .filter((e) => e);

  return (
    <TouchableOpacity
      style={[
        styles.defaultStyle,
        buttonType === 'primary' && styles.primary,
        buttonType === 'dangerous' && styles.dangerous,
        buttonType === 'warning' && styles.warning,
        disabled && styles.disabled,
        combinedStyle,
        margin && styles.margin(normalizeOptions(margin)),
        padding && styles.padding(normalizeOptions(padding)),
        style,
      ]}
      {...rest}>
      {prefix}
      <Box
        margin={[0, 24]}
        flex={1}
        flexDirection="row"
        justify="center"
        align="center"
        {...middleBox}>
        <Typography
          type="C3"
          color={
            disabled
              ? colors.ZT_normal_grey_2
              : buttonType !== 'default'
              ? colors.ZT_white
              : colors.ZT_black
          }
          fontStyle={buttonType !== 'default' ? 'bold' : undefined}>
          {title}
        </Typography>
        {loading && (
          <Box margin={[0, 4]}>
            <ActivityIndicator color={colors.ZT_white} />
          </Box>
        )}
      </Box>
      {suffix}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  background: (color) => {
    return { backgroundColor: color };
  },
  width: (width) => {
    return { width };
  },
  height: (height) => {
    return { height };
  },
  flex: (number) => {
    return { flex: number };
  },
  flexDirection: (direction) => {
    return {
      flexDirection: direction,
    };
  },
  justify: (alignment) => {
    return {
      justifyContent: alignment,
    };
  },
  align: (alignment) => {
    return {
      alignItems: alignment,
    };
  },
  alignSelf: (alignment) => {
    return {
      alignSelf: alignment,
    };
  },
  square: (number) => {
    return {
      height: number,
      width: number,
    };
  },
  circle: (number) => {
    return {
      height: number,
      width: number,
      borderRadius: number / 2,
    };
  },
  shadowDepth: (depth) => {
    let realDepth = 0;
    if (depth > 24) {
      realDepth = 24;
    } else if (depth < 0) {
      realDepth = 0;
    } else {
      realDepth = depth;
    }

    return {
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: SHADOW_HEIGHT_MAP[realDepth],
      },
      shadowOpacity: SHADOW_OPACITY_MAP[realDepth],
      shadowRadius: SHADOW_RADIUS_MAP[realDepth],
      elevation: realDepth,
    };
  },
  margin: ([top, left, bottom, right]) => {
    return {
      marginTop: top,
      marginBottom: bottom,
      marginLeft: left,
      marginRight: right,
    };
  },
  padding: ([top, left, bottom, right]) => {
    return {
      paddingTop: top,
      paddingBottom: bottom,
      paddingLeft: left,
      paddingRight: right,
    };
  },
  defaultStyle: {
    height: 48,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.ZT_normal_grey_1,
  },
  primary: {
    borderWidth: 0,
    borderColor: 'transparent',
    backgroundColor: colors.ZT_green,
  },
  dangerous: {
    borderWidth: 0,
    borderColor: 'transparent',
    backgroundColor: colors.ZT_red,
  },
  warning: {
    borderWidth: 0,
    borderColor: 'transparent',
    backgroundColor: colors.ZT_orange,
  },
  disabled: {
    height: 40,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.ZT_normal_grey_2,
    backgroundColor: colors.ZT_light_grey_1,
  },
});

export default ZTButton;
