import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from '@src/screens/SplashScreen';
// import Drawer from './Drawer';
import RootStack from './Stack';

const Stack = createStackNavigator();

const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="RootStack" headerMode={'none'}>
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          options={null}
        />
        <Stack.Screen
          name="RootStack"
          component={RootStack}
          options={{ gestureEnabled: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
