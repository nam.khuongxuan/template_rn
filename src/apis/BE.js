import TokenManagement from '@src/apis/TokenManagement';
import ZotaPmsStore from '@src/store';
import { extend } from 'umi-request';

// DEV;
// export const BE = extend({
//   prefix: 'http://apigw.dev-zotahome.com',
// });

// STG
export const BE = extend({
  prefix: 'http://apigw.stg-zotahome.com',
});

// PROD
// export const BE = extend({
//   prefix: 'http://apigw.stg-zotahome.com',
// });

// DEV;
// export const AUTH = extend({
//   prefix: 'http://auth.dev-zotahome.com',
// });

// STG;
export const AUTH = extend({
  prefix: 'http://auth.stg-zotahome.com',
});

// PROD;
// export const AUTH = extend({
//   prefix: 'https://auth.zotahome.com',
// });

//ZENDESK
export const ZENDESK = extend({
  prefix: 'https://zo-ta.zendesk.com',
});

BE.interceptors.response.use(
  async (response) => {
    // const data = await response.clone().json();
    if (!response.ok) {
      // console.error(data);

      return response;
    }

    return response;
  },
  { global: false },
);

export const injectBearer = (token, configs) => {
  const idOrg = ZotaPmsStore?.store?.getState()?.auth?.selectedOrg?.idOrgUnit;
  if (!configs) {
    return {
      headers: {
        Authorization: `Bearer ${token}`,
        'x-organization-id': idOrg,
        'X-Language': 'VI',
      },
    };
  }

  if (configs.headers) {
    return {
      ...configs,
      headers: {
        ...configs.headers,
        Authorization: `Bearer ${token}`,
        'x-organization-id': idOrg,
        'X-Language': 'VI',
      },
    };
  }
  return {
    ...configs,
    headers: {
      Authorization: `Bearer ${token}`,
      'x-organization-id': idOrg,
      'X-Language': 'VI',
    },
  };
};

const TokenManager = new TokenManagement({
  isTokenValid: () => {
    return true;
  },
  getAccessToken: () => {
    return ZotaPmsStore?.store?.getState()?.auth?.orgSession?.access_token;
  },
});

export const privateRequest = async (request, url, configs) => {
  const token = await TokenManager.getToken();
  return request(url, injectBearer(token, configs));
};

const PROFILE_COMPOSITE = '/profile-composite/v1.0';
const ORG_SERVICE = '/organizational-unit-configuration-service/v1.0';
const AUTHORIZATION_SERVICE = '/authorization-server/api/v1';
const LISTING_SERVICE = '/listing-service/v1.0';
const PROPERTY_SERVICE = '/property-composite/v1.0';
const LEAD_SERVICE = '/lead-service/v1.0';
export const PATHS = {
  // org
  PUBLIC_ORG_UNIT_FIND: `${ORG_SERVICE}/asset-manager/org-unit-config/find`,
  // authorization
  RESEND_TOKEN: `${AUTHORIZATION_SERVICE}/oauth/token/re-send`,
  CHAT_TOKEN: (id) =>
    `${AUTHORIZATION_SERVICE}/oauth/token/get-stream-token/${id}`,
  SIGN_UP: `${AUTHORIZATION_SERVICE}/composite/user/sign-up`,
  CONFIRM_PASSWORD: `${AUTHORIZATION_SERVICE}/security/password/encrypt`,

  // profile
  SIGN_IN: `${PROFILE_COMPOSITE}/auth/sign-in`,
  BIO_SIGN_IN: `${PROFILE_COMPOSITE}/auth/bio/sign-in`,
  ROLES_INFO: `${PROFILE_COMPOSITE}/producer/customer-role/find`,
  SELF_ROLES_INFO: `${PROFILE_COMPOSITE}/producer/self-get-role`,
  SELF_INFO: `${PROFILE_COMPOSITE}/producer/self-get`,
  PROPERTY_FIND_PRODUCER_CONTACT: `${PROFILE_COMPOSITE}/producer/contact/find`,

  // property
  PROPERTY_FIND_ASSETS: `${PROPERTY_SERVICE}/asset-manager/asset/find`,
  PROPERTY_DETAIL: (id) => `${PROPERTY_SERVICE}/asset-manager/asset/${id}`,
  PROPERTY_DETAIL_FIND_ASSET_ATTACHMENT: (id) =>
    `${PROPERTY_SERVICE}/asset-manager/asset/${id}/attachment/find`,
  PROPERTY_FIND_UNITS: `${PROPERTY_SERVICE}/asset-manager/unit/find`,
  PROPERTY_UPDATE_UNITS: (unit) =>
    `${PROPERTY_SERVICE}/asset-manager/unit/${unit.id}/asset/${unit.idAsset}`,
  PROPERTY_ASSET_TYPES: `${PROPERTY_SERVICE}/asset/type`,
  PROPERTY_RENTAL_TYPES: `${PROPERTY_SERVICE}/asset/rental-type`,
  PROPERTY_DURATIONS: `${PROPERTY_SERVICE}/duration/find`,
  PROPERTY_FIND_CURRENT_LEASES: `${PROPERTY_SERVICE}/asset-manager/lease/current-lease`,
  PROPERTY_FIND_LEASE_TENANTS: `${PROPERTY_SERVICE}/asset-manager/lease/tenants/find`,
  PROPERTY_FIND_FEE_TEMPLATE: (idAccount) =>
    `${PROPERTY_SERVICE}/${idAccount}/fee-template/find`,
  PROPERTY_LEASE_CREATE: (idAsset, idUnit) =>
    `${PROPERTY_SERVICE}/asset-manager/lease/asset/${idAsset}/unit/${idUnit}`,
  PROPERTY_TENANT_LEASE_CREATE: (idLease) =>
    `${PROPERTY_SERVICE}/asset-manager/lease/${idLease}/lease-tenant-infos`,
  PROPERTY_FEE_CREATE: (idLease) =>
    `${PROPERTY_SERVICE}/asset-manager/lease/${idLease}/fee`,
  PROPERTY_ATTACHMENT_CREATE: (idLease, type) =>
    `${PROPERTY_SERVICE}/asset-manager/lease/${idLease}/attachment/${type}`,
  PROPERTY_LEASE_EXECUTE: (idLease) =>
    `${PROPERTY_SERVICE}/asset-manager/lease/${idLease}/execute`,
  PROPERTY_LEASE_TENANT_IDENTITY_CARD_BACK: (idAsset, idUnit) =>
    `${PROPERTY_SERVICE}/asset-manager/lease/${idAsset}/lease-tenant-info/${idUnit}/attachment/IDENTITY_CARD_BACK`,
  PROPERTY_LEASE_TENANT_IDENTITY_CARD_FRONT: (idAsset, idUnit) =>
    `${PROPERTY_SERVICE}/asset-manager/lease/${idAsset}/lease-tenant-info/${idUnit}/attachment/IDENTITY_CARD_FRONT`,
  // listing
  LISTING_DIRECTION: `${LISTING_SERVICE}/directions`,
  LISTING_RENTAL_TYPES_FOR_ASSET: `${LISTING_SERVICE}/rentalTypes/forAsset`,
  LISTING_RENTAL_TYPES_FOR_POST: `${LISTING_SERVICE}/rentalTypes/forPost`,
  LISTING_PACKAGES: `${LISTING_SERVICE}/packages`,
  LISTING_FILTER_POSTS: `${LISTING_SERVICE}/posts/filter`,
  GET_ATTRIBUTES_MAP_API: `${LISTING_SERVICE}/mappedAttributes`,
  GET_RENTAL_TYPES: `${LISTING_SERVICE}/rentalTypes`,
  // location
  LOCATION_FIND: `${PROPERTY_SERVICE}/location/find`,

  // lease
  LEASE_DETAIL: (id) => `${PROPERTY_SERVICE}/asset-manager/lease/${id}`,
  LEASE_FIND: `${PROPERTY_SERVICE}/asset-manager/lease/find`,

  //fee
  FEE_PERIODIC_FIND: (idLease) =>
    `${PROPERTY_SERVICE}/asset-manager/lease/${idLease}/fee-periodic/find`,

  //Unit
  UNIT_DETAIL: (idAsset, id) =>
    `${PROPERTY_SERVICE}/asset-manager/unit/${id}/asset/${idAsset}`,
  UNIT_DETAIL_AMENITIES: (id, idAsset) =>
    `${PROPERTY_SERVICE}/asset-manager/unit/${id}/asset/${idAsset}/attribute`,
  //ChangePassword
  // AUTH_SECURITY_CHANGE_PASSWORD: '/api/v1/security/change-password',
  UNIT_DETAIL_FIND_ATTACHMENT: (id, idAsset) =>
    `${PROPERTY_SERVICE}/asset-manager/unit/${id}/asset/${idAsset}/attachment/find`,

  UNIT_DETAIL_FIND_ADDITION_FEE: `${PROPERTY_SERVICE}/asset-manager/unit/addition-fee/find`,

  SUMMARY: `${LEAD_SERVICE}/asset-manager/leads/summary`,
  UPDATE_FOR_SALE_STATUS: `${PROPERTY_SERVICE}/asset-manager/unit/for-sale-status`,

  LEAD_DETAIL: (leadId) => `${LEAD_SERVICE}/asset-manager/leads/${leadId}`,

  LEAD_SEARCH: (page, pageSize) =>
    `${LEAD_SERVICE}/asset-manager/leads/search?page=${page}&pageSize=${pageSize}`,

  ATTRIBUTE_MAP: `${PROPERTY_SERVICE}/attribute/map/find`,

  ADD_NOTE_LEAD: `${LEAD_SERVICE}/asset-manager/leads/addNote`,
};

export const AUTH_PATHS = {
  FORGOT_PASSWORD: '/api/v1/security/forgot-password',
  AUTH_SECURITY_CHANGE_PASSWORD: '/api/v1/security/change-password',
};

export const ZENDESK_PATHS = {
  ZENDESK_CATEGORIES_360001512414:
    '/api/v2/help_center/en-us/categories/360001512414/sections.json',
  ZENDESK_SESSION_ARTICLES: (idArticles) =>
    `/api/v2/help_center/en-us/sections/${idArticles}/articles.json`,
  ZENDESK_ARTICLES_JSON: (id) =>
    `/api/v2/help_center/en-us/articles/${id}.json`,
  ZENDESK_CATEGORIES_360001490994:
    '/api/v2/help_center/en-us/categories/360001490994/sections.json',
};
