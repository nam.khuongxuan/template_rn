import React from 'react';
import Typography from '@src/components/Typography';
import TouchableBox from '@src/components/TouchableBox';
import { Alert } from 'react-native';
import Box from '@src/components/Box';
const Home = ({ navigation }) => {
  return (
    <Box flex={1} align="center" justify="center">
      <TouchableBox onPress={() => Alert.alert('Thông báo', 'AbacAbacAbac')}>
        <Typography type="C4">Home</Typography>
      </TouchableBox>
      <TouchableBox onPress={() => navigation.navigate('ModalAlert')}>
        <Typography type="C4">ModalAlert</Typography>
      </TouchableBox>
    </Box>
  );
};

export default Home;
